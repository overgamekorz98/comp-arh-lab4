﻿using System;
using System.IO;
namespace AK4
{

    class CopyFiles
    {
        static int Main(string[] args)
        {
            args = new string[] { "C:\\Лабораторнi\\ПЗ-22у-1\\КоробковЄвгенОлександрович\\batch\\ПрихованийКаталог", "C:\\НовийКаталог"  };

            if (args.Length != 2)
            {
                Console.WriteLine("Usage: CopyFiles <source directory> <destination directory>");
                return 1;
            }

            string sourceDir = args[0];
            string destDir = args[1];
            string filePattern = "*.txt";

            int numFilesCopied = 0;

            foreach (string filePath in Directory.EnumerateFiles(sourceDir, filePattern))
            {
                string fileName = Path.GetFileName(filePath);
                string destPath = Path.Combine(destDir, fileName);

                try
                {
                    File.Copy(filePath, destPath, true);
                    numFilesCopied++;
                    FileAttributes attributes = File.GetAttributes(destPath);

                    // set hidden, read-only and archive attributes based on source file
                    FileAttributes sourceAttributes = File.GetAttributes(filePath);
                    if ((sourceAttributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                    {
                        attributes |= FileAttributes.Hidden;
                    }
                    if ((sourceAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        attributes |= FileAttributes.ReadOnly;
                    }
                    if ((sourceAttributes & FileAttributes.Archive) == FileAttributes.Archive)
                    {
                        attributes |= FileAttributes.Archive;
                    }
                    File.SetAttributes(destPath, attributes);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to copy {fileName}: {ex.Message}");
                }
            }

            Console.WriteLine($"Copied {numFilesCopied} files.");
            return 0;
        }
    }
}